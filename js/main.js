(() => {
  var e,
    t = 0,
    n = document.getElementById("nav");
  window.addEventListener("scroll", function (r) {
    (e = window.scrollY),
      t < e && e > 1800
        ? (n.classList.remove("slide-down"), n.classList.add("slide-up"))
        : (n.classList.remove("slide-up"), n.classList.add("slide-down")),
      (t = e);
  });
  $(window).on("load", function () {
    $(".loader-wrapper").fadeOut("slow");
  });
  $("#mtoggle").on("click", function (e) {
    1 == document.getElementById("menuToggle").checked
      ? ($("body").removeClass("noscroll"),
        (document.getElementById("menuToggle").checked = !0))
      : ($("body").addClass("noscroll"),
        (document.getElementById("menuToggle").checked = !1));
  });
  $(".key-nav").on("click", function (e) {
    (document.getElementById("menuToggle").checked = !1),
      $("body").removeClass("noscroll");
  });
})();
$(".hero-list").slick({
  slidesToShow: 1,
  dots: true,
  autoplaySpeed: 3000,
  infinite: true,
  autoplay: true,
  pauseOnHover: false,
  cssEase: "ease-out",
  fade: true,
});
$(".project-list").slick({
  slidesToShow: 1,
  dots: true,
  autoplaySpeed: 3000,
  infinite: true,
  autoplay: true,
  pauseOnHover: false,
  cssEase: "linear",
});
$(".client-list").slick({
  slidesToShow: 6,
  dots: true,
  responsive: [
    {
      breakpoint: 920,
      settings: {
        slidesToShow: 3,
      },
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        centerMode: true,
        centerPadding: "80px",
      },
    },
  ],
});
$(".post-list").slick({
  slidesToShow: 4,
  // dots: true,
  responsive: [
    {
      breakpoint: 920,
      settings: {
        slidesToShow: 2,
      },
    },
    {
      breakpoint: 480,
      settings: {
        variableWidth: true,
        slidesToShow: 1,
      },
    },
  ],
});
